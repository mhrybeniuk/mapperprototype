//
//  OptionalProtocol.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/12/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import Foundation

/// Is used in mappable object for defining type of wrapped value.
protocol OptionalProtocol {
    static var wrappedType: Any.Type { get }
}

extension Optional : OptionalProtocol {
    static var wrappedType: Any.Type { return Wrapped.self }
}
