//
//  MappableObjectObserver.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/15/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import Foundation


/// Is used for observing property changing in mappable object.
class MappableObjectObserver : NSObject  {
    
    var currentValues : [String : Any]
    
    init(with values: [String : Any]) {
        currentValues = values
    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        
//        if let keyPath = keyPath, let change = change {
//            
//            if let new =  change[NSKeyValueChangeKey.newKey] {
//                currentValues[keyPath] = new
//            }
//            else {
//                currentValues.removeValue(forKey: keyPath)
//            }
//            
//        }
//        
//    }
    
}
