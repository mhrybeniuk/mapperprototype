//
//  AppDelegate.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/12/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import UIKit
import FMDB

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window : UIWindow?
    var db : FMDatabase!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        let dbPath = "tmp.db"
//
//        let fileManager = FileManager.default
//
//        do {
//            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
//            let fileURL = documentDirectory.appendingPathComponent(dbPath)
//            db = FMDatabase(url: fileURL)
//        } catch {
//            print(error)
//        }
//        
//        if !db.open() {
//            fatalError("Database cannot be opened")
//        }
//
//        try? db.executeUpdate("create table person (firstName text, lastName text, age integer, sex text)", values: nil)
//
//        db.beginTransaction()
//
//        db.executeUpdate("insert into person (firstName, lastName, age, sex) values (?, ?, ?, ?)", withArgumentsIn: ["Ivan", "Ivanov", 25, "Male"])
//
//        db.commit()
//
//
//
//
//        if let result = db.executeQuery("select * from person", withArgumentsIn: []) {
//
//            result.next()
//
//            if let dictionary = result.resultDictionary as? [String : Any] {
//                let person = Person(values: dictionary, defaultValues: [:])
//                print(person)
//            }
//
//        }
//
//        db.close()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

