//
//  Person.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/12/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import Foundation

class Person : MappableObject {
    
    @objc dynamic var firstName : String = ""
    @objc dynamic var lastName : String = ""
    @objc dynamic var age : Int = 0
    @objc dynamic var sex : String = ""

}

class Worker : Person {
    
    @objc dynamic var workPlace : String?
    
}
