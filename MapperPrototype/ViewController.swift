//
//  ViewController.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/12/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let personDictionary : [String : Any] = ["age" : 15, "lastName" : "Ivanov", "firstName" : "Ivan"]
        let defaultValues : [String : Any] = ["sex" : "male"]
        
//        let objectPerson = Worker(values: personDictionary, defaultValues: defaultValues)
//
//        print(objectPerson.dictionary())
//
//        objectPerson.workPlace = "CoreValue"
//
//        print(objectPerson.dictionary())
        
        var arrayPersons = [Person]()
        
        NSLog("Start")
        
        for _ in 0..<30000 {
            arrayPersons.append(Person(values: personDictionary, defaultValues: defaultValues))
        }
        
        NSLog("End")
        print(arrayPersons.count)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

