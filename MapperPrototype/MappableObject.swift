//
//  Mapper.swift
//  MapperPrototype
//
//  Created by Mykola Hrybeniuk on 1/12/18.
//  Copyright © 2018 Mykola Hrybeniuk. All rights reserved.
//

import Foundation

/// Main functionality of the class is mapping from two dictionaries. First dictionary is current values, and second one is defaults values.
/// Inherited class should have next format:
//
//  class Person : MappableObject {
//      @objc dynamic var firstName : String = ""
//      @objc dynamic var lastName : String = ""
//      @objc dynamic var age : Int = 0
//      @objc dynamic var sex : String?
//  }
class MappableObject : NSObject {
    
    private enum MappingError : Error {
        case invalidTypes(errorMessage: String)
    }
    
    /// Convenient description of the object. Print all properties of inherited classes.
    override var description: String {
        let reflectionObject = Mirror(reflecting: self)
        
        var info = super.description
        
        if reflectionObject.subjectType == MappableObject.self {
            return info
        }
        
        info = info + "\n{"
        for (name, value) in unwrap(from: Mirror(reflecting: self)) {
            info = info + "\n   " + "\(name): \(type(of: value)) = '\(value)'"
        }
        info = info + "\n}"
        
        return info
    }
    
    private var kvoContext: UInt8 = 1
    
    /// Observable object observes any changes of properites and saves it into dictionary.
    private var observer = NSObject()
    private var valuesObservation = [NSKeyValueObservation]()
    
    
    /// Create an instance from dictionaries. The dicitionries is merged into one, where dictionary "values" has higher priority.
    ///
    /// - Parameters:
    ///   - values: [key : value] key is name of property, value should be the same type as the property
    ///   - defaultValues: [key : value] key is name of property, value should be the same type as the property
    init(values: [String : Any], defaultValues: [String : Any]) {
     
        var mergedWithDefaults = values
        
        mergedWithDefaults.merge(defaultValues) { (currentValue, defaultValue) -> Any in
            return currentValue
        }
        
        observer = MappableObjectObserver(with: mergedWithDefaults)
        
        super.init()
        
        do {
            try decode(from: mergedWithDefaults)
        } catch MappingError.invalidTypes(let errorMesage) {
            print(errorMesage)
            fatalError()
        } catch {
            print(error)
        }
        
    }

    
    /// Decodes input dictionary into properties of self. Uses reflection.
    ///
    /// - Parameter values: Input dictionry where key is name of property, value is a value for the property.
    private func decode(from values: [String : Any]) throws {

        for (name, value) in unwrap(from: Mirror(reflecting: self)) {
            
            if let valueForKey = values[name] {
                
                if type(of:valueForKey) == type(of: value) {
                    
                    setValue(valueForKey, forKey: name)
                    
                }
                else if let optionalType = value as? OptionalProtocol {
                    
                    if type(of: optionalType).wrappedType == type(of:valueForKey) {
                        
                        setValue(valueForKey, forKey: name)
                        
                    }
                    else {
                        throw MappingError.invalidTypes(errorMessage: "Incompatible optional types for property "+name+" with type \(type(of: value)) and set value type of \(type(of: valueForKey))")
                    }
                    
                }
                else {
                    throw MappingError.invalidTypes(errorMessage: "Incompatible types for property "+name+" with type \(type(of: value)) and set value type of \(type(of: valueForKey))")
                }
                
            }
            else if !(value is OptionalProtocol) {
                throw MappingError.invalidTypes(errorMessage: "Nil is not acceptable for non-optional property "+name)
            }
            
            addObserver(observer, forKeyPath: name, options: .new, context: &kvoContext)

//            let keyPath = \MappableObject.description
//            let observation = observe(keyPath) { object, change in
//                print(object)
//            }
//            valuesObservation.append(observation)
            
        }

    }
    
    /// Unwrap properties from mirror object into array of tupels (name, value).
    ///
    /// - Parameter reflectionObject: reflected object
    /// - Returns: array of tupels (name, values)
    private func unwrap(from reflectionObject: Mirror) -> [(String, Any)] {
        var properties = [(String, Any)]()

        for (name, value) in reflectionObject.children {
            guard let name = name else { continue }
            properties.append((name, value))
        }
        
        if reflectionObject.superclassMirror?.subjectType != MappableObject.self { // Recursively get properties from superclass. But stop at the root class MappableObject.
            properties.append(contentsOf: unwrap(from: reflectionObject.superclassMirror!))
        }
        
        return properties
        
    }
    
    subscript(key: String) -> Any? {
        get {
            return value(forKey: key)
        }
        set(value) {
            setValue(value, forKey: key)
        }
    }
    
    /// Updated dictionary with current values of self.
    ///
    /// - Returns: Dictionary with key = name of property, value
//    func dictionary() -> [String : Any] {
//        return observer.currentValues
//    }
    
    deinit {
//        for (name, _) in unwrap(from: Mirror(reflecting: self)) {
//            removeObserver(observer, forKeyPath: name)
//        }
        
    }
    
}
